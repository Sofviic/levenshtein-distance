# Levenshtein Distance

trivial implementation in haskell.

might change it later to work for all `Traversable`(eg `Tree`).\
ie `:: (Traversable t, Eq a) => t a -> t a -> Int`.

see the [wikipedia page](https://en.wikipedia.org/wiki/Levenshtein_distance).

## Usage
It checks the edit distance between 2 lists (eg strings).\
for example:
```hs
λ> levenshtein "mispeling" "misspelling"
2
λ> levenshtein "germany" "deutschland"
8
λ> levenshtein "france" "frankriek"
4
λ> levenshtein [0,1,1,2,3,5,8] [1,2,4,8,16,32]
6
λ> levenshtein [0,1,1,2,3,5,8] [1,1,2,3,5,8,13]
2
```

## License
```
Copyright (C) 2024 sof

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
```

