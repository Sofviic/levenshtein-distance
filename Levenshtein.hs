module Levenshtein where

levenshtein :: Eq a => [a] -> [a] -> Int
levenshtein a      []     = length a
levenshtein []     b      = length b
levenshtein (a:as) (b:bs) | a == b = levenshtein as bs
                          | a /= b = 1 + minimum [ levenshtein as     (b:bs),
                                                   levenshtein (a:as) bs    ,
                                                   levenshtein as     bs    
                                                 ]
